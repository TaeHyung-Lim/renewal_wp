

// <!--start add_member js-->
function EnNumCheck(word) {
    var str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
    for (i = 0; i < word.length; i++) {
        idcheck = word.charAt(i);
        for (j = 0; j < str.length; j++) {
            if (idcheck == str.charAt(j)) break;
            if (j + 1 == str.length) {
                return false;
            }
        }
    }
    return true;
}

function isNumber(s) {
    var regu = "^[0-9]+$";
    var re = new RegExp(regu);
    if (s.search(re) != -1) {
        return true;
    } else {
        return false;
    }
}


// 숫자만 입력받는다. "-"도 받지않는다.
function onlyNumber(loc,num,max) {
	num= num ? num : 1;
	max = max ? max : 999999999999999999;
	if(/[^0123456789]/g.test(loc.value)) {
		alert("숫자가 아닙니다.\n\n0-9의 정수만 허용합니다.");
		loc.value = num;
		loc.focus();
	} else if (loc.value>max) {
		loc.value = max;
		alert("최대입력 숫자는 "+max+"입니다.");
	}
}

function onlynum(loc,num) {

	if(/[^0123456789]/g.test(loc.value)) {
		alert("숫자가 아닙니다.\n\n0-9의 정수만 허용합니다.");
		loc.value = num;
		loc.focus();
	}
}

function clear1(str){
	return str.replace(/,/g,'');
}
function clear2(str){
	return str.replace(/명/g,'');
}

function cknumber(nNumber) {
	nNumber = clear1(nNumber);
	var Number = '' + nNumber;
	var sSplit = new RegExp('([0-9])([0-9][0-9][0-9][,.])');
	var aNumber = Number.split('.');
	aNumber[0] += '.';
	do {
	aNumber[0] = aNumber[0].replace(sSplit, '$1,$2');
	}
	while (sSplit.test(aNumber[0]));
	return aNumber[0].split('.')[0];
}

function keyCheck() {
    if (event.keyCode < 48 || event.keyCode > 57) {
        alert("키값이은 48~57사이만 가능합니다.");
        event.returnValue = false;
    }
}

function showid() {
	alert("명함과 신분증 사본을 팩스로 보내주시면 \n\n확인후 승인처리되며 즉시 고객상담이 가능합니다.");
}

function commonuser() {
	alert("딜러회원만 신청서 조회가능합니다.");
}

function checkEmail(strEmail) {
    // var emailReg = /^[_a-z0-9]+@([_a-z0-9]+\.)+[a-z0-9]{2,3}$/;
    var emailReg = /^[\w-]+(\.[\w-]+)*@[\w-]+(\.[\w-]+)+$/;
    if (emailReg.test(strEmail)) {
        return true;
    } else {
        return false;
    }
}

function gourl(vl) {
	location.href=vl;
}

//셀렉트박스 체크 함수
function ckselect(vid,msg) {
	var value= $("#"+vid).val();
	alert(value);
}

// 빈값 체크 함수
function cknull(vid,msg) {
	if ($("#"+vid).val()=="") {
		if (msg=="file") {
			alert("파일을 첨부해 주세요.");
		} else {
			alert(msg+" 입력해 주세요.");
		}
		$("#"+vid).focus();
		return "x";
	} else {
		return "o";
	}
}

// 라디오버튼 체크 함수
function ckrdo(vid,msg) {
	var rdo=$("input[name="+vid+"]:checked");
	//alert(rdo.length);
	if(rdo.length < 1) {
		alert(msg+" 입력해 주세요.");
		$("#"+vid).focus();
		return "x";
	} else {
		return "o";
	}

}

// 빈값 체크 함수
function cknull_size(vid,msg,mins,maxl) {
	min = (mins=="") ? 0 : parseInt(mins);
	max = (maxl=="") ? 10000000 : parseInt(maxl);
	if ($("#"+vid).val()=="" || $("#"+vid).val().length < min || $("#"+vid).val().length > max ) {
		if ($("#"+vid).val().length < min && min!=0) {
			alert(min+"자 이상의 "+msg+" 입력해 주세요.");
		} else if ($("#"+vid).val().length > maxl && min!=10000000) {
			alert(max+"자 이하의 "+msg+" 입력해 주세요.");
		} else {
			alert(minl+"자이상 "+maxl+"자이하의 "+msg+" 입력해 주세요.");
		}
		$("#"+vid).focus();
		return "x";
	} else {
		return "o";
	}
}

// 숫자인빈값체크
function cknullInt(vid,msg) {
	if ($("#"+vid).val()=="" || isNumber($("#"+vid).val()) == false) {
		$("#"+vid).focus();
		alert(msg+" 입력해 주세요.");
		return "x";
	} else {
		return "o";
	}
}

// o x 체크 중복확인체크
function ckox(vid,msg) {
	if ($("#"+vid).val()!="o") {
		alert(msg);
		$("#zongbuc").focus();
		return "x";
	} else {
		return "o";
	}
}

// 아이디 체크
function ckid(vid,msg,minl,maxl) {
	if ($("#"+vid).val()=="" || $("#"+vid).val().length < minl || $("#"+vid).val().length > maxl || EnNumCheck($("#"+vid).val()) == false) {
		alert(minl+"자이상 "+maxl+"자이하의 영문 숫자 "+msg+" 입력해 주세요.");
		$("#"+vid).focus();
		return "x";
	} else {
		return "o";
	}
}

// 비밀번호 확인 체크 함수
function ckpw(vid,ckid) {
	if ($("#"+vid).val()!=$("#"+ckid).val()) {
		alert("입력하신 비밀번호가 불일치합니다. 다시입력해 주시길바랍니다.");
		$("#"+vid).val('');
		$("#"+ckid).val('');
		$("#"+vid).focus();
		return "x";
	} else {
		return "o";
	}
}

// 이메일 확인
function ckemail(email1,email2) {
	var email = $("#"+email1).val()+"@"+$("#"+email2).val();
	if (checkEmail(email) == false) {
		alert("정확한이메일정보를 입력해 주세요");
		$("#"+email1).focus();
		return "x";
	} else {
		return "o";
	}
}

// 전화번호 체크
function ckphone(phone1,phone2,msg) {
	if ($("#"+phone1).val()=="" || isNumber($("#"+phone1).val()) == false) {
		alert("정확한"+msg+" 입력해 주세요");
		$("#"+phone1).focus();
		return "x";
	} else if ($("#"+phone2).val()=="" || isNumber($("#"+phone2).val()) == false) {
		alert("정확한"+msg+" 입력해 주세요");
		$("#"+phone2).focus();
		return "x";
	} else {
		return "o";
	}
}

// 주소체크
function ckaddr(zip1,zip2,addr1,addr2) {
	if ($("#"+zip1).val()=="" || $("#"+zip2).val()=="" || $("#"+addr1).val()=="") {
		alert("정확한 우편번호 정보를 선택해 주세요.");
		$("#"+zip1).focus();
		return "x";
	} else if ($("#"+addr2).val()=="") {
		alert("상세주소 정보를 입력해 주세요.");
		$("#"+addr2).focus();
		return "x";
	} else {
		return "o";
	}
}

// 팝업레이어창 띄우기 닫기
function pop_layer(id,tp) {
	if (tp=="o") {
		$("#"+id).show();
	} else {
		$("#"+id).hide();
	}
}

// <!--Start login js-->
function login() {
    var frm = document.loginform;

    if ((frm.user_id.value.length == 0) || (frm.user_id.value.length < 4)) {
        alert("아이디를 입력해 주세요.");
        frm.user_id.select();
        frm.user_id.focus();
        return false;
    } else if (EnNumCheck(frm.user_id.value) == false) {
        alert("정확한 아이디를 입력해 주세요.");
        frm.user_id.focus();
        return false;
    }


    if (frm.pw.value.replace(/ /g, '') == "") {
        alert("비밀번호를 입력해 주세요.");
        frm.pw.focus();
        return false;
    }
    return true;
}

// 우편번호 창

function win_open(url, name, option) {
    var popup = window.open(url, name, option);
    popup.focus();
}

function win_zip(frm_name, frm_zip1, frm_zip2, frm_addr1, frm_addr2) {
    url = "post.php?frm_name=" + frm_name + "&frm_zip1=" + frm_zip1 + "&frm_zip2=" + frm_zip2 + "&frm_addr1=" + frm_addr1 + "&frm_addr2=" + frm_addr2;
    win_open(url, "winZip", "left=50,top=50,width=508,height=600,scrollbars=no");
}

function awin_zip(frm_name, frm_zip1, frm_zip2, frm_addr1, frm_addr2,url) {
    url = url+"main/post.php?frm_name=" + frm_name + "&frm_zip1=" + frm_zip1 + "&frm_zip2=" + frm_zip2 + "&frm_addr1=" + frm_addr1 + "&frm_addr2=" + frm_addr2;
    win_open(url, "winZip", "left=50,top=50,width=508,height=508,scrollbars=no");
}
