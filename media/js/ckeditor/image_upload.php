<?php

if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_FILES) && $_FILES['upload']['size'] > 0) {
  $path =  $_GET['path'];
  $uploadpath  = $_SERVER['DOCUMENT_ROOT']."/v1/uploads/".$path;
  $uploadsrc = $_SERVER['HTTP_HOST']."/v1/uploads/".$path;
  $http = 'http' . ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS']=='on') ? 's' : '') . '://';

  $filename = $_FILES['upload']['name'];
  $tmpname = $_FILES['upload']['tmp_name'];

  $date_filedir = date("YmdHis");
  $ext = substr(strrchr($_FILES["upload"]["name"],"."),1);
  $ext = strtolower($ext);
  $savefilename = $date_filedir."_".str_replace(" ", "_", $filename);

  //php 파일업로드하는 부분
  if ($ext == 'jpg' || $ext == 'gif' || $ext == 'png') {
    if(move_uploaded_file($_FILES['upload']['tmp_name'],$uploadpath."/".iconv("UTF-8","EUC-KR",$savefilename))){
      $uploadfile = $savefilename;

      $url = "://".$_SERVER['SERVER_NAME'] ."/ckeditor_fileupload/uploads/".$_FILES['upload']['name'];

      $function_number = $_GET['CKEditorFuncNum'];
      $url = $http.$uploadsrc.'/'.$uploadfile;
      $message = '';
      echo "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction($function_number, '$url', '$message');</script>";
    }else{
      echo "<script type='text/javascript'>alert('파일 업로드에 실패했습니다.');</script>";
    }
  }else{
    echo "<script type='text/javascript'>alert('jpg, gif, png 파일만 업로드가 가능합니다.');</script>";
  }

  exit;
}
/*
  if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_FILES) && $_FILES['upload']['size'] > 0) {
    $path = "board_image";
    $path = str_replace('../', '', $path).'/';
    $uploadsrc = '/test_dev/uploads/'.$path;
    $uploadpath = $_SERVER['DOCUMENT_ROOT'].$uploadsrc;

    if (!is_dir($uploadpath)){
      mkdir($uploadpath​, 0707);
      chmod($uploadpath, 0707);
    }

    $filename = $_FILES['upload']['name'];
    $tmpname = $_FILES​['upload']['tmp_name'];

    $ext = strtolower(substr(strrchr($filename,'.'), 1));
    $date_filedir = date("YmdHis");
    $filename = $date_filedir."_".str_replace(" ", "_", $filename);

    $savefilename = $uploadpath.$filename;
    $uploaded_file = '';

    if ($ext == 'jpg' || $ext == 'gif' || $ext == 'png') {
      $image_info​ = getimagesize($tmpname);
      if ($image_info['mime'] == 'image/png' || $image_info['mime'] == 'image/jpeg' || $image_info['mime'] == 'image/gif') {
        if (move_uploaded_file($tmpname, iconv('UTF-8', 'EUC-KR', $savefilename))) {
          $uploaded_file = $filename;
        }
      } else {
        echo json_encode(array(
          'uploaded'=>'0',
          'error'=>array('message'=>'이미지 파일의 형식이 올바르지 않습니다.')
        ));
        exit;
      }
    } else {
      echo json_encode(array(
        'uploaded'=>'0',
        'error'=>array('message'=>'jpg, gif, png 파일만 업로드가 가능합니다.')
      ));
      exit;
    }

  } else {
    echo json_encode(array(
      'uploaded'=>'0',
      'error'=>array('message'=>'업로드중 문제가 발생하였습니다.')
    ));
  exit;
}

echo json_encode(array(
    'uploaded'=>'1',
    'fileName'=>$uploaded_file,
    'url'=>$uploadsrc​.$uploaded_file,
));

exit;*/
?>
