<?php

require('app/code/inc/common.php');

if (trim($_SESSION["ss_s_no"])) {
    goto_url(URLPATH."main.php");
}elseif (trim($_SESSION["ss_c_no"])) {
    goto_url(URLPATH."out_company/main.php");
}

goto_url(URLPATH."login.php");

?>
