<?php

ini_set('error_reporting',E_ALL & ~E_NOTICE | E_STRICT);

date_Default_TimeZone_set("Asia/Seoul");	// 시간설정

define('BASEPATH', $_SERVER['DOCUMENT_ROOT']);

define('CODEPATH', BASEPATH."/app/code/");

define('TEMPLATEPATH', BASEPATH."/app/design/");

define('URLPATH', "app/code/");

session_start();

require CODEPATH.'inc/config.php';
require CODEPATH.'inc/log.php';