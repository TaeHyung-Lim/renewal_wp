<?php
	extract($_POST);
	extract($_GET);

	// [DB 연결]
	$db_config_url = BASEPATH.'/libs/dbconfig.php';
	if(file_exists($db_config_url)) {
		require($db_config_url);
		if (trim($user)=="") {
			echo "libs/dbconfig.php 파일이 존재하지 않습니다.";
		}
	} else {
		echo "libs/dbconfig.php 파일이 존재하지 않습니다.";
		exit;
	}

	// [공통 function]
	require(CODEPATH.'inc/lib.php');

	// [스마티 클래스 연결 및 기본 설정]
	require(BASEPATH.'/libs/Smarty.class.php');
	$smarty 					= new Smarty;
	$smarty->left_delimiter		= "<{";
	$smarty->right_delimiter	= "}>";
	$smarty->template_dir		= TEMPLATEPATH."templates/";
	$smarty->compile_dir		= TEMPLATEPATH."templates_c/";
	$smarty->caching			= FALSE;
	$smarty->cache_dir			= BASEPATH."/var/cache/";
	$smarty->cache_lifetime		= 40;
	$smarty->compile_check 		= true;
?>
