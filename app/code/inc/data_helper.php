<?php

// 계열사 정보
$my_company_sql		= "SELECT mc.my_c_no, mc.ceo_s_no, mc.c_name, mc.kind_color, mc.company_number, mc.license_type, mc.active_state FROM my_company mc	WHERE mc.active_state='1'";
$my_company_list	= [];
$my_company_query 	= mysqli_query($my_db,$my_company_sql);
while($my_company_data = mysqli_fetch_array($my_company_query))
{
	$my_company_list[] = array
	(
		"my_c_no"		 => $my_company_data['my_c_no'],
		"ceo_s_no"		 => $my_company_data['ceo_s_no'],
		"c_name"		 => $my_company_data['c_name'],
		"kind_color"	 => $my_company_data['kind_color'],
		"company_number" => $my_company_data['company_number'],
		"license_type"	 => $my_company_data['license_type'],
		"active_state"	 => $my_company_data['active_state']
	);
}

$smarty->assign("my_company_list", $my_company_list);
?>