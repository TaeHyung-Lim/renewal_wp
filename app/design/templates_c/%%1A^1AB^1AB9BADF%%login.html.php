<?php /* Smarty version 2.6.26, created on 2020-01-17 16:03:43
         compiled from login.html */ ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
    <meta charset="utf-8"/>
    <title>로그인</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta content="" name="description"/>
    <meta content="" name="author"/>
    <!-- css -->
    <link href="../design/css/default.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="../design/css/font-awesome.min.css">
    <!--// css -->
</head>

<body style="background:#e5e9ec;">
<form name="loginform" method="post" action="login.php">
    <table style="margin-left: auto; margin-right: auto; margin-top:20px;" cellpadding="0" cellspacing="0">
        <tr>
            <td style="text-align:center;">
                <img src="../design/images/wplanet_logo.png"/><br><br>
                <div style="text-align:center;font-size:45px; font-weight:600; color:#666">업무관리 시스템</div>
                <div style="text-align:center;font-size:21px;margin-top:10px; color:#666">Work Management System</div>
                <div style="text-align:right;font-size:12px;margin-top:10px; color:#666">(WMS ver 1.0.1)</div>
            </td>
        </tr>
        <tr>
            <td>
                <table style="margin-left: auto; margin-right: auto;" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <div class="m-b-5" style="margin-top:20px;"><label class="idpw">COMPANY</label></div>
                            <select id="my_c_no" name="my_c_no" style="width:310px; height: 30px; background-color: #ffddd1;">
                                <?php unset($this->_sections['data']);
$this->_sections['data']['name'] = 'data';
$this->_sections['data']['loop'] = is_array($_loop=$this->_tpl_vars['my_company_list']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data']['show'] = true;
$this->_sections['data']['max'] = $this->_sections['data']['loop'];
$this->_sections['data']['step'] = 1;
$this->_sections['data']['start'] = $this->_sections['data']['step'] > 0 ? 0 : $this->_sections['data']['loop']-1;
if ($this->_sections['data']['show']) {
    $this->_sections['data']['total'] = $this->_sections['data']['loop'];
    if ($this->_sections['data']['total'] == 0)
        $this->_sections['data']['show'] = false;
} else
    $this->_sections['data']['total'] = 0;
if ($this->_sections['data']['show']):

            for ($this->_sections['data']['index'] = $this->_sections['data']['start'], $this->_sections['data']['iteration'] = 1;
                 $this->_sections['data']['iteration'] <= $this->_sections['data']['total'];
                 $this->_sections['data']['index'] += $this->_sections['data']['step'], $this->_sections['data']['iteration']++):
$this->_sections['data']['rownum'] = $this->_sections['data']['iteration'];
$this->_sections['data']['index_prev'] = $this->_sections['data']['index'] - $this->_sections['data']['step'];
$this->_sections['data']['index_next'] = $this->_sections['data']['index'] + $this->_sections['data']['step'];
$this->_sections['data']['first']      = ($this->_sections['data']['iteration'] == 1);
$this->_sections['data']['last']       = ($this->_sections['data']['iteration'] == $this->_sections['data']['total']);
?>
                                	<option value="<?php echo $this->_tpl_vars['my_company_list'][$this->_sections['data']['index']]['my_c_no']; ?>
"><?php echo $this->_tpl_vars['my_company_list'][$this->_sections['data']['index']]['c_name']; ?>
</option>
                                <?php endfor; endif; ?>
                            </select>
                            <div class="m-b-5" style="margin-top:20px;"><label class="idpw">ID</label></div>
                            <input type="text" style="ime-mode:disabled" name="user_id" id="user_id" class="input_w_login w300px">
                            <div class="m-b-5" style="margin-top:20px;"><label class="idpw">Password</label></div>
                            <input type="password" name="password" id="password" class="input_w_login w300px"><br>
                            <div style="text-align:right;margin-top:20px;">
                                <button class="btn btn-green btn-cons" type="submit">Login</button>
                            </div>
                            <div style="margin-top:30px;">&nbsp;</div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</form>
</body>
</html>